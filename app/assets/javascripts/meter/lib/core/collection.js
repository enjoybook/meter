//= require ./event-broker
//= require ./listener
//= require ./model

Meter.Collection = Backbone.Collection.extend({
	model: Meter.Model,

	constructor: function () {
		Backbone.Collection.apply(this, arguments);

		// Декларативные слушатели
		this.delegateListeners();
	}

});

_.extend(Meter.Collection .prototype, Meter.eventBroker, Meter.Listener);