//= require ./event-broker
//= require ./listener

Meter.Model = Backbone.Model.extend({

	constructor: function () {
		Backbone.Model.apply(this, arguments);

		// Декларативные слушатели
		this.delegateListeners();
	},

	setSilent: function(obj) {
		this.set(obj, { silent: true });
	},

	unary: function (name, operation) {
		var value = this.get(name);
		this.set(name, operation(value));
	},

	toggleAttr: function (name) {
		this.unary(name, function (value) {
			return !value;
		});
	},

	inc: function (name) {
		this.unary(name, function (value) {
			return value + 1;
		});
	},

	dec: function (name) {
		this.unary(name, function (value) {
			return value - 1;
		});
	},

	dispose: function () {
		// todo: Закончить
		return;
		if (this.disposed) return;

		// Finished.
		this.disposed = true;

		// Unbind all global event handlers.
		this.unsubscribeAllEvents();

		// Unbind all referenced handlers.
		this.stopListening();

		// Remove all event handlers on this module.
		this.off();


		// Remove the collection reference, internal attribute hashes
		// and event handlers.
			var properties = [
				'collection',
				'attributes', 'changed',
				'_escapedAttributes', '_previousAttributes',
				'_silent', '_pending',
				'_callbacks'
			];

//			delete this[prop] for prop in properties
	}

});

_.extend(Meter.Model.prototype, Meter.eventBroker, Meter.Listener);