#= require ./event-broker
#= require ./listener

class Meter.Router extends Backbone.Router

	constructor: (options) ->
		super

		@delegateListeners();
		@_cache = {}
		@$parent = $(options.el)
		@$parent.on 'click', '.app-route', @_handleRoute
		@startHistory()


	startHistory: ->
		Backbone.history.start pushState: true


	_handleRoute: (event) =>
		event.preventDefault()
		# todo target
		route = event.currentTarget.getAttribute('data-route')
		@navigate(route, trigger: true)


	cache: (name, Ctor, args...) ->
		@_cache[name] ?= new Ctor(args...)


_.extend Meter.Router::, Meter.eventBroker, Meter.Listener

