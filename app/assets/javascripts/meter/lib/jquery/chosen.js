(function($) {

	// var settings = $.extend({}, options);

	var methods = {
		/**
		 * Initialize chosen plugin
		 */
		initialize: function() {
			this.$menu      = this.find('.menu'),
			this.$container = this.find('.filter-container'),
			this.$items     = this.$menu.find('.item');

			this.chosen('setEvents');
			this.chosen('setDataIndex');
		},

		/**
		 * Set up events
		 */
		setEvents: function() {
			(function(_this){
				$('body').on('click', '.filter-container .filter-button', function(e) {
					e.stopImmediatePropagation();
					var $el = $(e.target);
					var text = e.target.innerText;
					var $item = $('.filter-form').find('.item:contains("' + text + '")');
					var index = $item.data('index');
					$el.remove();
					_this.chosen('removeActive', index);
				});

				_this.$container.on('click', function(e) {
					if (e.target.className === 'filter-container') {
						if (_this.$menu.hasClass('-unvisible')) {
							_this.chosen('showMenu');
						} else {
							_this.chosen('hideMenu');
						}
					}
				});

				/**
				 * On click an item the one will be added in
				 * the container or will be removed
				 */
				_this.$items.on('click', function() {
					var $el = $(this);

					if ($el.hasClass('-special')) {
						return false;
					}

					if ($el.hasClass('-active')) {
						$el.removeClass('-active');
						var index = $el.data('index');
						_this.chosen('removeElement', index);
					} else {
						$el.addClass('-active');
						var title = $el.text();
						var index = $el.data('index');

						_this.chosen('addElement', title, index);
					}
				});

				/**
				 * Hide menu on mouseleave
				 */
				_this.on('mouseleave', function() {
					_this.chosen('hideMenu');
				});

				/**
				 * The 'everything' event will be triggered on click
				 */
				_this.bind('everything', function() {
					_this.$items.each(function(i, el) {
						if (!$(el).hasClass('-active') &&
								!$(el).hasClass('-special')) {

							var title = $(el).text();
							var index = $(el).data('index');

							$(el).addClass('-active');

							_this.chosen('addElement', title, index);
						}
					});
				});

				/**
				 * The 'removeAll' event will be triggered on click
				 */
				_this.bind('removeAll', function() {
					$items_in_container = _this.$container.find('div');
					$items_in_container.each(function(i, el) {
						index = $(el).data('index');
						_this.chosen('removeElement', index);
					});

					_this.$items.each(function(i, el) {
						$(el).removeClass('-active');
					});
				});
			})(this);
		},

		/**
		 * Show menu
		 */
		showMenu: function() {
			this.$menu.removeClass("-unvisible");
		},

		/**
		 * Hide menu
		 */
		hideMenu: function() {
			this.$menu.addClass("-unvisible");
		},

		/**
		 * Add an element in the container
		 * @param {title} The title of an element
		 * @param {index} The attr for setting up data-index
		 */
		addElement: function( title, index ) {
			$el = '<div class="filter-button" data-index="' + index + '">' + title + '</div>';
			this.$container.append($el);
		},

		/**
		 * Remove an element from the container
		 * @param {index} The index of an element for removing
		 */
		removeElement: function( index ) {
			this.$container.find('div[data-index="' + index + '"]').remove();
		},

		/**
		 * Remove '-acitve' class on element
		 * @param {index} The index of an element for removing
		 */
		removeActive: function( index ) {
			$('.filter-form').find('.item[data-index="' + index + '"]').removeClass('-active');
		},

		/**
		 * Set up attr data-index on all items
		 */
		setDataIndex: function() {
			this.$items.each(function(i, el) {
				$(el).attr('data-index', (i+1));
			});
		},

		getCurrentItems: function() {
			$current_items = this.find('.menu').find('.item');
			items = $.map( $current_items, function(el) {
				return $(el).text();
			})

			return items;
		}
	};

	$.fn.chosen = function(method) {
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.initialize.apply( this, arguments );
		} else {
			$.error( 'Метод с именем ' +  method + ' не существует для chosen' );
		}
	}
})(jQuery);