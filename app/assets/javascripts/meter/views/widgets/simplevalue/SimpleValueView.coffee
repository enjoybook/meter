#= require meter/lib/core/view


class Meter.Views.SimpleValueView extends Meter.View

	template: HandlebarsTemplates['widgets/simplevalue/simple']

	listen:
		'@model change': 'render'
