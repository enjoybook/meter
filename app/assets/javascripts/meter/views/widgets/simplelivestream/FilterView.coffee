#= require meter/lib/core/view


class Meter.Views.FilterView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/simplelivestream/filter']

	listen:
		'@model change:categories': 'render'

	events:
		'click @item': 'changeOneFilter'
		'click @everything': 'changeFilters'
		'click .filter-button': 'removeFilter'

	_render: ->
		@$('@filter').chosen()


	changeFilters: ->
		$menu = @$('@menu')
		items_all = $menu.find('.item').length
		items_active = $menu.find('.-active').length

		$filter = @$('@filter')

		if items_all isnt items_active
			$filter.trigger 'everything'
			filters = $filter.chosen 'getCurrentItems'
		else
			@resetFilters()

		@model.set 'filters', filters || []


	changeOneFilter: (event) ->
		filters = @getCloneFilters()

		$item = $(event.target)
		filter = $item.text()

		if $item.hasClass '-active'
			filters.push filter
		else
			filters = _.without filters, filter

		@model.set 'filters', filters


	removeFilter: (e) ->
		filter = $(event.target).text()
		filters = _.without @getCloneFilters(), filter
		@model.set 'filters', filters


	resetFilters: ->
		@$('@filter').trigger 'removeAll'


	getCloneFilters: ->
		_.clone @model.get 'filters'
