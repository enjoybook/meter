#= require meter/lib/core/view


class Meter.Views.PieChartView extends Meter.View

	template: HandlebarsTemplates['widgets/charts/pie']

	_render: ->
		@renderChart()


	renderChart: ->
		model = @model

		@$('@pie').highcharts

			title:
				text: ''
				style:
					display: 'none'

			tooltip:
				pointFormat: '<b>{point.y:.1f}</b>'

			credits:
				enabled: false

			legend:
				labelFormat: '{name}<br>{y} ({percentage:.1f}%)',

			plotOptions:
				pie:
					allowPointSelect: true
					cursor: 'pointer'
					dataLabels:
						enabled: false
						formatter: ->
							"<b>#{@point.name}</b>: #{@y} (#{@percentage}%)"

					showInLegend: true

			series: [
				type: 'pie'
				name: @model.get 'title'
				data: @model.get 'data'
			]

			chart:
				events:
					load: ->
						[sort, data] = [model.get('sort'), model.get('data')]
						if data
							new_data = switch sort
								when 'direct' then _.sortBy data, (d) -> d[1]
								when 'reverse' then _.sortBy data, (d) -> -d[1]
								else data
							@series[0].setData new_data
