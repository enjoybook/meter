#= require meter/lib/core/view


class Meter.Views.LineChartView extends Meter.View

	template: HandlebarsTemplates['widgets/charts/line']

	_render: ->
		@renderChart()


	renderChart: ->
		@$('@line').highcharts

			title:
				text: ''
				style:
					display: 'none'

			xAxis:
				plotBands: @_setPlotBands()
				labels:
					rotation: 0
				categories: @model.get 'categories'

			yAxis: @model.get 'yaxis'

			tooltip:
				shared: true
				crosshairs: true
				formatter: ->
					point_texts = this.points.map (point) -> "#{point.series.name}: #{point.y}"
					"#{this.x}<br>#{point_texts.join '<br>'}"

			credits:
				enabled: false

			legend:
				enabled: true

			series: @model.get 'series'


	_setPlotBands: ->
		bands = @model.get 'bands'
		_.flatten _.map bands, (band) =>
			@_getPlotBand band


	_getPlotBand: (band) ->
		{ points, color } = band
		_.map points, (point) ->
			from = point - 0.5
			to = point + 0.5
			{ color, from, to }
