#= require meter/lib/core/view


class Meter.Views.ColumnChartView extends Meter.View

	template: HandlebarsTemplates['widgets/charts/column']

	_render: ->
		@renderChart()


	renderChart: ->
		@$('@column').highcharts

			title:
				text: ''
				style:
					display: 'none'

			credits:
				enabled: false

			yAxis: @model.get 'yaxis'

			xAxis:
				plotBands: @_setPlotBands()
				labels:
					rotation: 0
				categories: @model.get 'categories'

			tooltip:
				formatter: ->
					totals = _.map @series.data, (point) -> point.total
					# вынести
					sum = _.reduce totals, (memo, total) -> memo + total
					percent =  (@point.total / sum * 100).toFixed 2
					"#{@series.name}: <b>#{@point.y}</b> (#{percent}%)<br/>"

			plotOptions:
				column:
					stacking: 'normal'

			series: @model.get 'series'

			chart:
				type: 'column'
				backgroundColor: '#fff'
				style:
					fontFamily: "Dosis, sans-serif"


	_setPlotBands: ->
		bands = @model.get 'bands'
		_.flatten _.map bands, (band) =>
			@_getPlotBand band


	_getPlotBand: (band) ->
		{ points, color } = band
		_.map points, (point) ->
			from = point - 0.5
			to = point + 0.5
			{ color, from, to }
