#= require meter/lib/core/view


class Meter.Views.LiveStreamEventsView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/livestream-events']

	listen:
		'@model change': 'render'
