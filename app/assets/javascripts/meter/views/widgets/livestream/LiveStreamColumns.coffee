#= require meter/lib/core/view

#= require ./LiveStreamColumnView


class Meter.Views.LiveStreamColumns extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/livestream-header-view']

	listen:
		'@collection change': 'render'

	_render: ->
		@renderColumns()


	renderColumns: ->
		@collection.each (model) =>
			@renderColumn model


	renderColumn: (model) ->
		view = new Meter.Views.LiveStreamColumnView { model }
		@append '@livestream-header', view
