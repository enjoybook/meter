#= require meter/lib/core/view


class Meter.Views.LiveStreamAmountsView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/livestream-amounts']

	listen:
		'@model change': 'render'
