#= require meter/lib/core/view

#= require meter/models/livestream/LiveStreamAmountsModel
#= require meter/models/livestream/LiveStreamEventsModel
#= require meter/models/livestream/LiveStreamColumnsCollection

#= require ./LiveStreamColumns
#= require ./LiveStreamAmountsView
#= require ./LiveStreamEventsView


class Meter.Views.LiveStreamView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/livestream-view']

	events:
		'click @filter-arrow': 'showSelect'

	initialize: ->
		@initEvents()
		@initVariables()


	_render: ->
		@renderColumns()
		@renderEvents()
		@renderSumma()


	renderColumns: ->
		@liveStreamColumnsCollection = new Meter.Collections.LiveStreamColumnsCollection null, { @headers, @rows }
		@add '@livestream-table-header', Meter.Views.LiveStreamColumns, collection: @liveStreamColumnsCollection


	renderEvents: ->
		url = @model.get('url')
		@liveStreamEventsModel = new Meter.Models.LiveStreamEventsModel { @rows }, { url, @liveStreamColumnsCollection }
		@add '@livestream-events',  Meter.Views.LiveStreamEventsView, model: @liveStreamEventsModel


	renderSumma: ->
		liveStreamAmountsModel = new Meter.Models.LiveStreamAmountsModel null, { @rows, @liveStreamEventsModel }
		@add '@livestream-summa',  Meter.Views.LiveStreamAmountsView, model: liveStreamAmountsModel


	initVariables: ->
		@rows = @model.get 'rows'
		@headers = @model.get 'headers'


	initEvents: ->
		$(document).on 'click', (e) =>
			$el = $(e.target)
			unless $el.closest('@livestream-cell-filter').length
				@$('@livestream-cell-select').removeClass 'active'
				@$('@filter-arrow').removeClass 'active'


	showSelect: (e) ->
		$el = $(e.target)
		active = !!$el.hasClass 'active'

		@$('@filter-arrow').removeClass 'active'
		@$('@livestream-cell-select').removeClass 'active'

		unless active
			$el.addClass 'active'
			$el.parent().find('@livestream-cell-select').toggleClass 'active'
