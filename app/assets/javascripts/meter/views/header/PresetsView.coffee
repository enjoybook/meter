#= require meter/lib/core/view


class Meter.Views.PresetsView extends Meter.View

	template: HandlebarsTemplates['header/presets']

	events:
		'click @today': 'setToday'
		'click @yeasterday': 'setYesterday'
		'click @week': 'setWeek'
		'click @thirty': 'setThirtyDays'
		'click @lastweek': 'setLastWeek'
		'click @lastmonth': 'setLastMonth'
		'click @currentmonth': 'setCurrentMonth'
		'click @threemonths': 'setThreeMonths'
		'click @year': 'setYear'

	setToday: ->
		@model.setToday()


	setYesterday: ->
		@model.setYesterday()


	setWeek: ->
		@model.setWeek()


	setThirtyDays: ->
		@model.setThirtyDays()


	setLastWeek: ->
		@model.setLastWeek()


	setLastMonth: ->
		@model.setLastMonth()


	setCurrentMonth: ->
		@model.setCurrentMonth()


	setThreeMonths: ->
		@model.setThreeMonths()

	setYear: ->
		@model.setYear()