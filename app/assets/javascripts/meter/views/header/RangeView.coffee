#= require meter/lib/core/view


class Meter.Views.RangeView extends Meter.View

	template: HandlebarsTemplates['header/range']

	listen:
		'@model change': 'onDataUpdate'

	events:
		'click @range_datepicker': 'showOrHide'

	initialize: ->
		$(document).on 'click', (e) =>
			$el = $(e.target)

			unless $el.closest('@widget').length
				@hideCalendar()

	_render: ->
		@renderCalendar()


	renderCalendar: ->
		@$('@datePicker').DatePicker
			prev: '&#8656;'
			next: '&#8658;'
			flat: true
			format: @model.format
			date: ['', '']
			calendars: 3
			mode: 'range'
			onChange: =>
				@changeDate()


	renderRange: ->
		@updateRange()
		range = [@model.getFrom(), @model.getTo()]

		$('@datePicker').DatePickerSetDate range, true


	updateRange: ->
		@updateInterval()
		@updateRangeLine()


	updateRangeLine: ->
		@$('@range_datepicker').html @model.getRange()


	onDataUpdate: (model, options) ->
		@updateRange()
		unless options.fromPicker
			@renderRange()


	updateInterval: ->
		interval = @model.getInterval()
		@$('@range_interval_days').html interval


	changeDate: ->
		range = @$('@datePicker').DatePickerGetDate()
		[from, to] = [moment(range[0]), moment(range[1])]
		@model.set { from, to }, fromPicker: true


	showOrHide: ->
		visible = @$('#widgetCalendar').data 'show'

		unless visible then @showCalendar() else @hideCalendar()


	showCalendar: ->
		$calendar = @$('#widgetCalendar')

		$calendar.data 'show', true
		$calendar.slideDown()


	hideCalendar: ->
		$calendar = @$('#widgetCalendar')

		$calendar.data 'show', false
		$calendar.slideUp()