#= require meter/lib/core/view

#= require ./header/HeaderView
#= require ./WidgetsView
#= require ./details/DetailView
#= require ./SpinnerView


class Meter.Views.MeterView extends Meter.View

	template: HandlebarsTemplates['meter']

	listen:
		'@meterConfig sync': 'removeSpinner'

	initialize: (attributes, options) ->
		{ @rangeModel, @widgetsCollection, @meterConfig } = options


	_render: ->
		@addSpinner()
		@renderHeader()


	renderHeader: ->
		@add '@header', Meter.Views.HeaderView, { @widgetsCollection }


	addSpinner: ->
		@spinnerView = new Meter.Views.SpinnerView className: 'sk-fading-circle-general'
		@append '@spinner', @spinnerView


	removeSpinner: ->
		utils.timeoutCall => @spinnerView.remove()


	switchDashboard: ->
		widgetsView = new Meter.Views.WidgetsView collection: @widgetsCollection, { @rangeModel }
		@_switchView widgetsView


	switchWidget: (name) ->
		model = @getModelByName name
		detailView = new Meter.Views.DetailView { model, collection: @widgetsCollection }, { name, @rangeModel }
		detailView.setModel()
		@_switchView detailView


	_switchView: (view) ->
		@_view?.detach()
		# TODO: url is blinking without the following two commands
		@_view?.stopListening();
		@_view?.off();
		@_view = view
		@_view.delegateListeners()
		@append '@container', @_view


	getModelByName: (name) ->
		@widgetsCollection.findWhere { name }
