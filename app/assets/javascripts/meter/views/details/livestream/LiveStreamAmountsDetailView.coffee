#= require meter/lib/core/view


class Meter.Views.LiveStreamAmountsDetailView extends Meter.View

	template: HandlebarsTemplates['details/livestream/livestream-amounts']

	listen:
		'@model change': 'render'
