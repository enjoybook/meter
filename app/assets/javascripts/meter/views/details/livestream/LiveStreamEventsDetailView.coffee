#= require meter/lib/core/view


class Meter.Views.LiveStreamEventsDetailView extends Meter.View

	template: HandlebarsTemplates['details/livestream/livestream-events']

	listen:
		'@model change': 'render'