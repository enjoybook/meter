#= require meter/lib/core/view


class Meter.Views.StackedColumnChartDetailView extends Meter.View

	template: HandlebarsTemplates['details/stacked']

	listen:
		'@model change': 'render'

	_render: ->
		@renderChart()


	renderChart: ->
		hundred_percent = @model.get 'hundred_percent'

		@$('@stacked').highcharts

			title:
				text: ''
				style:
					display: 'none'

			credits:
				enabled: false

			yAxis:
				max: @_getMax(hundred_percent)
				title:
					text: ''
				plotLines: @_getPlotLines()

			xAxis:
				labels:
					rotation: 0
				categories: @model.get 'categories'

			tooltip:
				pointFormatter: ->
					"<span style='color:#{@series.color}'>#{@series.name}</span>: #{@y.toFixed(2)}% <b>(#{@series.options.stack_data.toFixed(2)})</b><br/>"
				shared: true

			plotOptions:
				column:
					stacking: 'normal'
					dataLabels:
						enabled: true
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						style:
							textShadow: '0 0 3px black'
						formatter: ->
							"<b>#{@point.y.toFixed(2)}%</b>"

			series: @model.get 'series'

			chart:
				type: 'column'
				backgroundColor: '#fff'
				style:
					fontFamily: "Dosis, sans-serif"
				events:
					load: ->
						if @series[0]
							_.each @series, (serie) ->
								opt = serie.options
								opt.stack_data = opt.data

							percentages = _.map @series, (serie) ->
								opt = serie.options
								percent = 100 * opt.data / hundred_percent
								percent

							_.each percentages, (percent, i) =>
								@series[i].setData [percent]


	_getMax: (hundred_percent) ->
		stacks_data = _.map @model.get('series'), (serie) -> serie.data
		if not _.isEmpty stacks_data
			summ = _.reduce stacks_data, (memo, num) -> memo += num
			res = (summ / hundred_percent * 100)
			res = 100 if res < 100
			res


	_getPlotLines: ->
		lines = @model.get 'lines'
		_.map lines, (line) ->
			color: "#ff0000"
			dashStyle: "dash"
			width: 1
			zIndex: 100
			value: line.value
			label:
				text: line.title
