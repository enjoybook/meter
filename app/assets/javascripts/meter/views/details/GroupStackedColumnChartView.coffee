#= require meter/lib/core/view


class Meter.Views.GroupStackedColumnChartDetailView extends Meter.View

	template: HandlebarsTemplates['details/group_stacked']

	_render: ->
		@renderChart()


	renderChart: ->
		formatter = @model.get 'formatter'

		@$('@group_stacked').highcharts

			title:
				text: ''
				style:
					display: 'none'

			credits:
				enabled: false

			yAxis:
				title:
					text: ''
				stackLabels:
					enabled: true
					style:
						fontWeight: 'bold',
						color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'

			xAxis:
				labels:
					rotation: 0
				categories: @model.get 'categories'
				plotBands: @_setPlotBands()

			tooltip:
				shared: true
				useHTML: true
				formatter: ->
					if formatter
						eval formatter
					else
						values = _.map @points, (point) ->
							"<span style=\'color: #{point.series.color}; \'>\u25CF</span> #{point.series.name} <b>#{point.y}</b>"
						values.join '<br />'

				positioner: (boxWidth, boxHeight, point) ->
					{ x: point.plotX, y: point.plotY }

			plotOptions:
				column:
					stacking: 'normal'
					dataLabels:
						enabled: true
						color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
						style:
							textShadow: '0 0 3px black'

			series: @model.get 'series'

			legend:
				labelFormatter: ->
					summ = _.reduce this.yData, ((acc, num) -> acc + num), 0
					"#{this.name} (#{summ})"

			chart:
				type: 'column'
				backgroundColor: '#fff'
				style:
					fontFamily: "Dosis, sans-serif"


	_setPlotBands: ->
		bands = @model.get 'bands'
		_.flatten _.map bands, (band) =>
			@_getPlotBand band


	_getPlotBand: (band) ->
		{ points, color } = band
		_.map points, (point) ->
			from = point - 0.5
			to = point + 0.5
			{ color, from, to }
