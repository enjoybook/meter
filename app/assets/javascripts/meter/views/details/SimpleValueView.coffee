#= require meter/lib/core/view


class Meter.Views.SimpleValueDetailView extends Meter.View

	className: 'simple__widget'

	template: HandlebarsTemplates['details/simple']
