#= require meter/lib/core/view


class Meter.Views.LiveStreamSimpleEventsDetailView extends Meter.View

	template: HandlebarsTemplates['widgets/livestream/simplelivestream/livestream-events']

	listen:
		'@model change': 'render'