#= require meter/lib/core/view


#= require meter/models/livestreamsimple/FilterModel
#= require ./FilterView

#= require meter/models/livestreamsimple/LiveStreamSimpleEventsModel
#= require ./LiveStreamSimpleEventsView


class Meter.Views.LiveStreamSimpleDetailView extends Meter.View

	template: HandlebarsTemplates['details/livestreamsimple']

	listen:
		'@model change': 'render'

	_render: ->
		@renderFilters()
		@renderEvents()


	renderFilters: ->
		rows = @model.get 'rows'
		@filterModel = new Meter.Models.FilterModel { rows }
		filterView = new Meter.Views.FilterDetailView model: @filterModel
		@append '@livestream-filter', filterView


	renderEvents: ->
		[url, rows] = [@model.get('url'), @model.get('rows')]
		@liveStreamSimpleEventsModel = new Meter.Models.LiveStreamSimpleEventsModel { rows }, { url, @filterModel }
		@add '@livestream-events', Meter.Views.LiveStreamSimpleEventsDetailView, model: @liveStreamSimpleEventsModel
