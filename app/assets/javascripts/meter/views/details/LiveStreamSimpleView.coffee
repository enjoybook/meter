#= require meter/lib/core/view


#= require meter/models/FilterModel
#= require meter/views/widgets/livestream/FilterView

#= require meter/models/livestream/LiveStreamEventsModel
#= require meter/views/widgets/livestream/LiveStreamEventsView


class Meter.Views.LiveStreamDetailView extends Meter.View

	template: HandlebarsTemplates['details/livestream']

	listen:
		'@model change': 'render'

	_render: ->
		@renderFilters()
		@renderEvents()


	renderFilters: ->
		rows = @model.get 'rows'
		@filterModel = new Meter.Models.FilterModel { rows }, categories: @model.get 'categories'
		filterView = new Meter.Views.FilterView model: @filterModel
		@append '@livestream-filter', filterView


	renderEvents: ->
		[url, rows] = [@model.get('url'), @model.get('rows')]
		@liveStreamEventsModel = new Meter.Models.LiveStreamEventsModel { rows }, { url, @filterModel }
		@add '@livestream-events', Meter.Views.LiveStreamEventsView, model: @liveStreamEventsModel
