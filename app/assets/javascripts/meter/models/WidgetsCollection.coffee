#= require meter/lib/core/collection
#= require meter/models/WidgetModel


class Meter.Collections.WidgetsCollection extends Meter.Collection

	url: window.meter_widgets_path

	model: Meter.Models.WidgetModel

	listen:
		'@ reset': 'getWidgetsData'
		'@rangeModel change': 'refreshWidgets'
		'@meterConfig change': 'getWidgetsConfig'

	initialize: (attributes, options) ->
		{ @rangeModel, @meterConfig } = options


	getWidgetsConfig: ->
		@reset @meterConfig.get 'charts'


	getWidgetsData: ->
		@each @getWidgetData.bind(this)


	getWidgetData: (model) ->
		@fetchModel model


	refreshWidgets: ->
		_.each @models, (model) =>
			model.from = @rangeModel.getFrom()
			model.to = @rangeModel.getTo()
			@fetchModel model


	fetchModel: (model) ->
		delay = @randomInRange 0, 1000
		setTimeout ->
			model.fetch
				error: (model, response, options) ->
					try
						data = $.parseJSON response.responseText
					catch
						data = {}
						data.title = 'Error'
						data.error = response.responseText
					data.isError = true
					model.set data
		, delay


	randomInRange: (min, max) ->
		Math.round Math.random() * (max - min) + min
