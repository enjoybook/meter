#= require meter/lib/core/model

class Meter.Models.QueryFormModel extends Meter.Model

	_key: 'query_form'


	initFormConfig: (queries) ->
		form_params = @get 'form_params'
		if form_params and not _.isEmpty form_params
			form_params = utils.merge form_params, queries
		else
			form_params = queries
		@setSilent { form_params }


	loadParamsFromUrl: ->
		query = @_getQueryFormFromUrl()
		return unless query
		queries = @_unwrapParams query[0]
		form_params = @_getQueryFormObjects queries
		@set { form_params }


	setFormParams: (paramsStr) ->
		params = @_parseParams paramsStr
		form_params = _.clone @get('form_params')
		form_params = utils.merge params, form_params
		form_params = @_wrapParams form_params unless @_isWrapped form_params
		@setSilent { form_params }


	getFormParams: ->
		form_params = @get 'form_params'
		params = if @_isWrapped(form_params) then form_params else @_wrapParams form_params
		@_getPreparedParams params


	_getQueryFormFromUrl: ->
		url('query').match /query_form%5B.*%5D=.*/g


	_unwrapParams: (query) ->
		query.replace(/(query_form%5B|%5D)/g, '').split '&'


	_getQueryFormObjects: (queries) ->
		_.map queries, (q) ->
			res = q.split '='
			obj = {}
			obj.name = res[0]
			obj.value = res[1]
			obj


	_getPreparedParams: (params) ->
		_.map params, (p) ->
			obj = {}
			obj[p.name] = p.value
			obj


	_wrapParams: (form_params) ->
		_.map form_params, (fp) =>
			fp.name = "#{@_key}[#{fp.name}]"
			fp


	_parseParams: (paramsStr) ->
		return unless paramsStr
		params = paramsStr.split '&'
		_.flatten _.map params, (param) ->
			p = _.zip param.split '='
			{ value: _.first p[1] }


	_isWrapped: (form_params) ->
		names = _.pluck form_params, 'name'
		_.every names, (name) => name.match ///#{@_key}\[.*\]///
