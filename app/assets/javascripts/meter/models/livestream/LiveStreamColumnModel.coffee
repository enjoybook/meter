#= require meter/lib/core/model


class Meter.Models.LiveStreamColumnModel extends Meter.Model

	defaults:
		id: 0
		sort: 'all'
		filters: [],
		title: '',
		arrow_status: 'no-elements'

	setNextSortStatus: ->
		sort = @getNextSortStatus @get 'sort'
		@set { sort }


	resetStatus: ->
		@set sort: 'all'


	getNextSortStatus: (currentStatus) ->
		switch currentStatus
			when 'all' then 'direct'
			when 'direct' then 'reverse'
			else 'all'


	setFilter: (id, filterName) ->
		filters = @get 'filters'
		filter = _.findWhere filters, value: filterName.toString()
		if filter.status is 'active'
			@deactivateFilter filter
		else
			@activateFilter filter


	activateFilter: (filter) ->
		@setStatus filter, 'active'


	deactivateFilter: (filter) ->
		@setStatus filter, 'noactive'


	setStatus: (filter, status) ->
		filters = utils.cloneDeep @get 'filters'
		filter = _.findWhere filters, value: filter.value
		filter.status = status
		arrow_status = if  @hasActiveFilter(filters) then 'have-elements' else 'no-elements'
		@set { filters, arrow_status }


	hasActiveFilter: (filters) ->
		!! _.findWhere filters, status: 'active'


