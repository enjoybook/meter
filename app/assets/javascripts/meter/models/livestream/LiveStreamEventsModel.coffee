#= require meter/lib/core/model


class Meter.Models.LiveStreamEventsModel extends Meter.Model

	defaults:
		filteredRows: []

	listen:
		'@liveStreamColumnsCollection change': 'changedRows'

	initialize: (attributes, options) ->
		@url_path = options.url
		{ @liveStreamColumnsCollection } = options
		@setRows()


	url: ->
		params = from:'', to: ''
		"#{@url}?#{$.param params}"


	changedRows: (changedModel) ->
		@setRows changedModel


	setRows: (changedModel) ->
		rows = @processRows @get('rows')
		filteredRows = @getRowsByFilters rows
		filteredRows = @sortRows filteredRows, changedModel
		formattedRows = @getRowsFormattedNumbers filteredRows
		@set { filteredRows, formattedRows }


	getRowsByFilters: (rows) ->
		_.each @liveStreamColumnsCollection.models, (model) =>
			rows = @getRowsByFilter model, rows
		rows


	processRows: (rows) ->
		_.map rows, (row) =>
			unless _.isNull row[0]
				row[0] = @processRow row[0]
			row


	processRow: (row) ->
		return null if typeof row[0] is 'object' or row[0] is ''
		return row if _.isNumber row
		row.toString().trim()


	getRowsByFilter: (model, rows) ->
		[id, filters] = [model.get('id'), model.get ('filters')]
		active_filters = @getActiveFilters filters
		if not _.isEmpty active_filters
			rows = _.filter rows, (row) =>
				if row[id] or _.isBoolean(row[id])
					value = utils.stripTags row[id].toString()
				else
					value = '[empty]'
				_.contains active_filters, value
		rows


	getActiveFilters: (filters) ->
		_.compact _.map filters, (filter) ->
			{status, value} = filter
			value if status is 'active'


	findIndex: (changedModel) ->
		{ models } = @liveStreamColumnsCollection
		_.indexOf _.pluck(models, 'cid'), changedModel.cid


	sortRows: (rows, model) ->
		unless _.isUndefined model
			[id, sortStatus] = [@findIndex(model), model.get('sort')]

		switch sortStatus
			when 'direct' then @sortBy rows, id
			when 'reverse' then @sortBy(rows, id).reverse()
			else rows


	getRowsFormattedNumbers: (rows) ->
		_.map rows, (row) ->
			_.map row, (r) ->
				return utils.formatNum(r) if r and utils.isNumber(r)
				r


	sortBy: (rows, id) ->
		arrs = _.partition rows, (row) -> _.isNull row[id]
		[nullRows, rows] = [arrs[0], arrs[1]]
		rows = _.sortBy rows, (r) -> r[id]
		rows.concat nullRows
