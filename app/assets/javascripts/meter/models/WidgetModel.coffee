#= require meter/lib/core/model


class Meter.Models.WidgetModel extends Meter.Model

	defaults:
		alreadySync: false
		isRendered: false
		title: '[Title]'
		yaxis:
			min: 0
			title:
				text: null

	listen:
		'@ sync': 'afterSync'

	initialize: (attributes, options) ->
		@rangeModel = @collection?.rangeModel || options.rangeModel


	url: ->
		url = @get 'url'
		data = url.split '?'
		connector = if data[1] then '&' else '?'
		"#{@get('url')}#{connector}#{@getParamsAsString()}"


	getParamsAsString: ->
		$.param @getParams()


	getParams: ->
		rangeParams = @getRangeParams()
		formParams = @getFormParams()
		utils.merge rangeParams, formParams


	getRangeParams: ->
		@rangeModel.getRangeParams()


	initQueryForm: ->
		@queryFormModel.initFormConfig @get 'queries'


	setQueryModel: ->
		return if @queryFormModel
		@queryFormModel = new Meter.Models.QueryFormModel


	getFormParams: ->
		return null unless @queryFormModel
		@queryFormModel.getFormParams()


	loadParamsFromUrl: ->
		@queryFormModel.loadParamsFromUrl()


	updateParamsFromUrl: ->
		@queryFormModel.updateParamsFromUrl()


	setFormParams: (params)->
		@queryFormModel.setFormParams params


	afterSync: ->
		@setQueryModel() unless @queryFormModel
		@initQueryForm() unless @get 'alreadySync'
		@setSilent { alreadySync: true }
