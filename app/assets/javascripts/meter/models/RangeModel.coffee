#= require meter/lib/core/model


class Meter.Models.RangeModel extends Meter.Model

	defaults:
		measure: 'auto'

	format: 'd.m.Y'

	listen:
		'@meterConfig change': 'setRange'

	initialize: (attributes, options) ->
		{ @meterConfig } = options


	setRange: ->
		query = url '?'
		from = @meterConfig.get 'from'
		to = @meterConfig.get 'to'
		measure = if query then query.measure else 'auto'
		utcOffset = @meterConfig.get 'utcOffset'

		@set
			from: @getDateWithoutUtc from
			to: @getDateWithoutUtc to
			measure: measure
			utcOffset: utcOffset


	getRangeParams: ->
		from: @formatDate @get('from')
		to: @formatDate @get('to')
		measure: @get('measure')


	getInterval: ->
		[to, from] = [@get('to'), @get('from')]
		if from and to
			diff = to.diff from
			duration = parseInt moment.duration(diff).asDays()
			if duration < 0 then duration = 0
			duration += 1


	getTo: ->
		@formatDate @get('to') if @get 'to'


	getFrom: ->
		@formatDate @get('from') if @get 'from'


	getRange: ->
		@getFrom() + ' - ' + @getTo()


	getDay: (number) ->
		@subtractDate number, 'days'


	getCurrentDate: ->
		utcOffset = @get 'utcOffset'
		moment().utcOffset(utcOffset)


	getDateWithoutUtc: (date) ->
		moment.parseZone date


	getMonthAgo: (months) ->
		month = @subtractDate months, 'months'
		month.startOf 'month'


	setToday: ->
		today = @getCurrentDate()
		@setDate today, today


	setYesterday: ->
		yesterday = @getDay 1
		@setDate yesterday, yesterday


	setWeek: ->
		startDay = @getDay 6
		endDate = @getCurrentDate()
		@setDate startDay, endDate


	setThirtyDays: ->
		startDay = @getDay 29
		endDate = @getCurrentDate()
		@setDate startDay, endDate


	getLastWeekStart: ->
		week = @subtractDate 1, 'weeks'
		week.startOf 'week'


	getLastWeekEnd: ->
		week = @subtractDate 1, 'weeks'
		week.endOf 'week'


	setLastWeek: ->
		startDay = @getLastWeekStart()
		endDate = @getLastWeekEnd()
		@setDate startDay, endDate


	getLastMonthStart: ->
		month = @subtractDate 1, 'months'
		month.startOf 'month'


	getLastMonthEnd: ->
		month = @subtractDate 1, 'months'
		month.endOf 'month'


	setLastMonth: ->
		startDate = @getLastMonthStart()
		endDate = @getLastMonthEnd()
		@setDate startDate, endDate


	setCurrentMonth: ->
		startDate = moment().startOf 'month'
		endDate = @getCurrentDate()
		@setDate startDate, endDate


	setThreeMonths: ->
		startDate = @getMonthAgo 2
		endDate = @getCurrentDate()
		@setDate startDate, endDate


	setYear: ->
		startDate = @subtractDate 12, 'months'
		endDate = @getCurrentDate()
		@setDate startDate, endDate


	setDate: (from, to) ->
		@set { from, to }


	formatDate: (date) ->
		date.format 'DD.MM.YYYY'


	subtractDate: (count, measure) ->
		utcOffset = @get 'utcOffset'
		moment().utcOffset(utcOffset).subtract count, measure
