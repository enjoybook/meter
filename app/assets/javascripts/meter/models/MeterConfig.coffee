#= require meter/lib/core/model


class Meter.Models.MeterConfig extends Meter.Model

	defaults:
		from: ''
		to: ''
		charts: []
		utcOffset: 0

	initialize: (attributes, options) ->
		@url = "#{window.meter_widgets_path}?#{@getParsedParams()}"


	getParsedParams: ->
		params = @getParams()
		$.param params if params


	getParams: ->
		@query = url '?'
		if @query
			from: @query.from
			to: @query.to
