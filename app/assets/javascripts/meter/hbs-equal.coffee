Handlebars.registerHelper 'equal', (v1, v2, options) ->
	return unless v1
	return options.fn(this) if v1.toString() is v2.toString()
	options.inverse this
