window.utils =

	toNum: (val) ->
		+val


	cloneDeep: (array) ->
		_.map array, _.clone


	transposeMatrix: (rows) ->
		_.zip.apply null, rows


	timeoutCall: (callback) ->
		setTimeout ->
			callback()
		, 0


	merge: (x, y) ->
		return x unless y
		if _.isArray(x) and _.isArray(y)
			return _.map y, (el, i) -> _.extend el, x[i]
		else if _.isArray(y)
			_.each y, (el) -> _.extend x, el
		else
			_.extend x, y
		x


	stripTags: (str) ->
		str.replace /<\/?[^>]+(>|$)/g, ''


	formatNum: (value) ->
		return 0 if _.isNaN value
		[integer, decimal] = value.toString().split '.'
		integer = integer.replace /\B(?=(\d{3})+(?!\d))/g, ' '
		return integer unless decimal
		"#{integer}.#{decimal}"


	isNumber: (val) ->
		return if val is ''
		return if _.isBoolean val
		not isNaN(+val) and isFinite val

