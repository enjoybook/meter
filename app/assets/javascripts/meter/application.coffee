#= require handlebars
#= require ./hbs-inc
#= require ./hbs-equal
#= require_tree ./templates

#= require meter/utils
#= require meter/Meter
#= require meter/lib/jquery/jquery-1.11.0
#= require meter/lib/underscore-1.6.0
#= require meter/lib/backbone-1.1.2
#= require meter/MainRouter

#= require meter/lib/moment
#= require meter/lib/jquery/url
#= require meter/lib/ru-locale
#= require meter/lib/jquery/chosen
#= require meter/lib/jquery/datepicker
#= require meter/lib/jquery/jquery.role
#= require meter/lib/highcharts/highcharts
#= require meter/lib/highcharts/exporting
#= require meter/lib/highcharts/export-csv

#= require meter/models/MeterConfig
#= require meter/models/RangeModel
#= require meter/models/WidgetsCollection
#= require meter/views/MeterView

meterConfig = new Meter.Models.MeterConfig
meterConfig.fetch()

rangeModel = new Meter.Models.RangeModel null, { meterConfig }

widgetsCollection = new Meter.Collections.WidgetsCollection null, { rangeModel, meterConfig }

meterView = new Meter.Views.MeterView { el: '@meter'}, { widgetsCollection, rangeModel, meterConfig }
router = new Meter.MainRouter { el: '@meter'}, { meterView }
