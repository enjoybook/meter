class WidgetsController < ApplicationController
  include ActionView::Helpers::UrlHelper

  def show
    from, to = from_to_range
    data = {
      from: from,
      to: to,
      utcOffset: Time.zone.utc_offset / 60 / 60,
      charts: [
        {
          name: 'linechart1',
          type: :linechart,
          url:  '/widgets/linechart1'
        }, {
          name: 'linechart2',
          type: :linechart,
          url:  '/widgets/linechart2'
        }, {
          name: 'linechart3',
          type: :linechart,
          url:  '/widgets/linechart3'
        }, {
          name: 'piechart1',
          type: :piechart,
          url:  '/widgets/piechart1'
        }, {
          name: 'piechart2',
          type: :piechart,
          url:  '/widgets/piechart2'
        }, {
          name: 'columnchart1',
          type: :columnchart,
          url:  '/widgets/columnchart1'
        }, {
          name: 'columnchart2',
          type: :columnchart,
          url:  '/widgets/columnchart2'
        }, {
          name: 'stacked',
          type: :stacked_column,
          url:  '/widgets/stacked'
        }, {
          name: 'simplevalue1',
          type: :simplevalue,
          url:  '/widgets/simplevalue1'
        }, {
          name: 'livestream1',
          type: :livestream,
          url:  '/widgets/livestream1'
        }, {
          name: 'livestream2',
          type: :livestream,
          url:  '/widgets/livestream2'
        }, {
          name: 'advanced',
          type: :livestream,
          url:  '/widgets/advanced'
        }, {
          name: 'advanced2',
          type: :livestream,
          url:  '/widgets/advanced2'
        }, {
          name: 'advanced3',
          type: :livestream,
          url:  '/widgets/advanced3'
        }, {
          name: 'binder1',
          type: :livestream,
          url:  "/widgets/binder?admin_id=1"
        }, {
          name: 'binder2',
          type: :livestream,
          url:  "/widgets/binder?admin_id=2&somevalue=4"
        }, {
          name: 'advanced4',
          type: :livestream,
          url:  '/widgets/advanced4'
        }, {
          name: 'error',
          type: :livestream,
          url: '/widgets/error'
        }, {
          name: 'simplelivestream',
          type: :simplelivestream,
          url:  '/widgets/simplelivestream'
        }, {
          name: 'group_stacked',
          type: :group_stacked_column,
          url:  '/widgets/group_stacked'
        }
      ]
    }

    render json: data
  end

  def proxy_analytics
    begin
      render json: analytics.public_send(params[:name], params)
    rescue => error
      render json: {
        title: 'Error',
        error: "#{error}, #{error.backtrace.join('<br/>')}"
      }, status: 500
    end
  end

  private

  def analytics
    from, to = from_to_range
    measure = get_measure from, to
    periods = from_to_periods from, to, measure
    Analytics.new from, to, measure, periods
  end

  def from_to_range
    from, to = if params.has_key?(:from)
      [Date.parse(params[:from]), Date.parse(params[:to])]
    else
      week = 6.day
      today = Time.current
      [today - week, today]
    end

    [
      Time.zone.local(from.year, from.month, from.day, 0, 0, 0),
      Time.zone.local(to.year,   to.month,   to.day, 23, 59, 59)
    ]
  end

  def get_measure(from, to)
    if params[:measure] == 'auto'
      get_measure_auto from, to
    else
      params[:measure]
    end
  end

  def get_measure_auto(from, to)
    days = days_from_to from, to
    two_weeks = 14
    two_months = 60

    case
      when days < two_weeks
        'days'
      when days < two_months
        'weeks'
      else
        'months'
    end
  end

  def from_to_periods(from, to, measure)
    case measure
      when 'days'
        from_to_days from, to
      when 'weeks'
        from_to_weeks from, to
      when 'months'
        from_to_months from, to
      else
        from_to_days from, to
    end
  end

  def days_from_to(from, to)
    (to.to_datetime - from.to_datetime).to_i + 1
  end

  def from_to_days(from, to)
    res = []
    one_day = 1.day

    while from < to
      res << [from, from + one_day - 1.second]
      from += one_day
    end

    res
  end

  def from_to_weeks(from, to)
    # 3 состояния
    # вт-вс, пн-вс, ..., пн-вс, пн-ср

    res = []

    # Первая неделя может начинаться не с Пн
    week_start = from
    # Может быть выбрано меньше недели
    week_end = [from.end_of_week, to].min

    # целые недели
    while week_end <= to
      res << [week_start, week_end]
      week_start = (week_start + 1.week).beginning_of_week
      week_end = (week_end + 1.week).end_of_week
    end

    # прибавляем кусок недели до to, если нужно
    if week_end > to and week_start < to
      res << [week_start, to]
    end

    res
  end

  def from_to_months(from, to)
    res = []

    month_start = from
    month_end = [from.end_of_month, to].min

    while month_end <= to
      res << [month_start, month_end]
      month_start = (month_start + 1.month).beginning_of_month
      month_end = (month_end + 1.month).end_of_month
    end

    if month_end > to and month_start < to
      res << [month_start, to]
    end

    res
  end
end