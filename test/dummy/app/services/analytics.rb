class Analytics

  include ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::NumberHelper

  def initialize(from, to, measure, periods)
    @from, @to, @measure, @periods = from, to, measure, periods
  end

  def linechart1(params)
    data = {
      title: "MONEYY",
      bands: [{
        color: '#FCFFC5',
        points: [1, 3]
      }],
      series: [
        {
          name: 'выручка',
          data: [4500, 7500, 8000, 10000, 7500],
        }
      ],
      queries: [
        {
          name: :test_name,
          title: 'Деньги',
          options: [4500, 7500, 8000],
          value: 7500
        },
        {
          name: :test_name2,
          title: 'Сроки',
          value: ''
        },
        {
          name: :test_name3,
          title: 'Тип доставки',
          value: ''
        },
        {
          name: :test_name4,
          title: 'Просто длинная надпись',
          value: ''
        }
      ]
    }
    spinnerTestWrapper data
  end

  def linechart2(params)
    data = {
      title: 'Money',
      bands: [{
        color: '#FCFFC5',
        points: [1, 3]
      }],
      categories: ['май', 'янв', 'март'],
      series: [
        {
          name: 'Выручка',
          data: [1,2,4]
        }, {
          name: 'Прибыль',
          data: [5,2,7]
        }
      ]
    }
    spinnerTestWrapper data
  end

  # + todo в книгах recent
  def linechart3(params)
    periods = extract_dates_from_periods
    columns = get_columns periods

    {
      title: 'Money',
      bands: [
        {
          points: columns,
          color: '#FCFFC5'
        }
      ],
      categories: formatted_periods,
      series: [
        {
          name: 'Выручка',
          data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4]
        }
      ]
    }
  end

  def piechart1(params)
    from = params[:from]

    if from == "04.03.2015"
      {
        :title => 'Browsers',
        :data => [
          ['Firefox', 25.0],
          ['IE',      26.8],
          ['Chrome',  12.8],
          ['Safari',  8.5 ],
          ['Opera',   6.2 ],
          ['Others',  20.7]
        ]
      }
    else
      {
        :title => 'Browsers',
        :data => [
          ['Firefox', 23],
          ['IE',      5]
        ]
      }
    end
  end

  def piechart2(params)
    {
      :sort => 'direct',
      :title => 'Browsers',
      :data => [
        ['Value-8', 10],
        ['Value-2', 2500],
        ['Value-6', 50],
        ['Value-3', 2000],
        ['Value-7', 20],
        ['Value-9', 10],
        ['Value-11', 10],
        ['Value-4', 250],
        ['Value-10', 15],
        ['Value-12', 10],
        ['Value-1', 5000],
        ['Value-13', 10],
        ['Value-5', 100],
        ['Value-14', 5]
      ]
    }
  end

  def columnchart1(params)
    from = params[:from]

    if from == "04.03.2015"
      {
        :title  => 'OLD COLUMNS',
        :categories => ['Seattle HQ', 'Russia', 'Tokyo'],
        :bands => [{
          color: '#FCFFC5',
          points: [4, 6]
        }],
        :yaxis => [
          {
            title: { text: 'yAxis I' }
          },
          {
            labels: { format: '{value} countries' },
            title: { text: 'yAxis II' },
            opposite: true
          }
        ],
        :series => [
          {
            :name => 'countries',
            :data => [150, 73, 20],
            :yAxis => 1
          }
        ]
      }
    else
      {
        :title  => 'NEW COLUMNS',
        :categories => ['Seattle HQ', 'Russia', 'Tokyo', 'Seattle HQ', 'Russia', 'Tokyo', 'Seattle HQ', 'Russia', 'Tokyo'],
        :bands => [{
          color: '#FCFFC5',
          points: [1, 3]
        }],
        :yaxis => [
          {
            title: { text: 'yAxis I' }
          },
          {
            title: { text: 'yAxis II' },
            labels: { format: '{value} countries' },
            opposite: true
          }
        ],
        :series => [
          {
            :name => 'countries',
            :data => [150, 73, 20, 150, 73, 20, 150, 73, 20].reverse!,
            :yAxis => 1
          }
        ]
      }
    end
  end

  def columnchart2(params)
    {
      :title  => 'NEW COLUMNS 2',
      :categories => (0..12).to_a,
      :series => [
        {
          :name => 'countries',
          :data => [0, 0, 0, 0, 4, 40, 30, 7, 7, 1, 1, 0]
        }
      ]
    }
  end

  def stacked(params)
    hundred_percent = 226845.82
    data = [537060.5, 286845.82, 275424.79, 60000.0, 47944.0, 45487.740000000005, 34871.27, 26568.87, 16879.2, 11980.0, 6774.66, 3823.33, 3500.0, 2079.78, 1400.0].reverse!

    series = data.each_with_index.map do |x, i|
      {
        name: "Расход-#{i}",
        data: data[i]
      }
    end

    {
      lines: [
        { value: 100, title: 'smth1' },
        { value: 75, title: 'smth2' }
      ],
      hundred_percent: hundred_percent,
      title: 'STACKED-COLUMN',
      categories: ['Приход / расход'],
      series: series
    }
  end

  def simplevalue1(params)
    from = params[:from]

    if from == "04.03.2015"
      {
        title: 'TOTAL ORDERS',
        value: 'Value 228<br>Next line',
        style: 'font-size: 20px'
      }
    else
      {
        :title => 'TOTAL ORDERS',
        value: 'Value 114<br>Next line',
        style: 'font-size: 20px'
      }
    end
  end

  def livestream1(params)
    now = formatDate DateTime.now

    {
      title: 'Статусы',
      headers: ['Тип', 'Описание', 'Дата'],
      rows: [
        ['ready', 'Заказ 13', now],
        ['sent',  'Заказ 12', now],
        ['recent','Заказ 14', now],
        ['sent',  'Заказ 13', now]
      ]
    }
  end

  def livestream2(params)
    now = formatDate DateTime.now

    {
      title: 'Статусы 22',
      headers: ['Тип', 'Описание', 'Дата', 'Фейк'],
      rows: [
        ['status1', "#{link_to 'Статус 1', '/meter'}", now, 'fake-1'],
        ['status1', 'Статус 1', now, 'fake-2'],
        ['status3', 'Статус 3', now, 'fake-3'],
        ['status4', 'Статус 4', now, 'fake-4'],
        ['status5', 'Статус 5', now, 'fake-5'],
        ['status7', 'Статус 7', now, 'fake-7'],
        ['status8', 'Статус 8', now, 'fake-8'],
        ['status9', 'Статус 9', now, 'fake-9'],
        ['status10', 'Статус 10', now, 'fake-10'],
        ['status8', 'Статус 8', now, 'fake-8'],
        ['status11', 'Статус 11', now, 'fake-11'],
        ['status12', 'Статус 12', now, 'fake-12']
      ]
    }
  end

  def advanced(params)
    now = formatDate DateTime.now

    data = {
      title: 'advanced',
      headers: ['Тип', 'Описание', 'Дата', 'Фейк', 'Количество'],
      rows: [
        ['4', 'Статус 1', now, 'fake-1', 1],
        ['1', 'Статус 9', now, 'fake-2', 1],
        ['3', 'Статус 3', now, 'fake-3', 1],
        ['1', 'Статус 4', now, 'fake-4', 1],
        ['8', 'Статус 5', now, 'fake-5', 1],
        ['7', 'Статус 7', now, 'fake-7', 1],
        ['5', 'Статус 8', now, 'fake-8', 1],
        ['9', 'Статус 1', now, 'fake-9', 1],
        ['8', 'Статус 8', now, 'fake-8', 1],
        ['4', 'Статус 1', now, 'fake-1', 1],
        ['1', 'Статус 1', now, 'fake-2', 5],
        ['3', 'Статус 3', now, 'fake-3', 5],
        ['1', 'Статус 4', now, 'fake-4', 5],
        ['8', 'Статус 5', now, 'fake-5', 5],
        ['7', 'Статус 7', now, 'fake-7', 5],
        ['5', 'Статус 8', now, 'fake-8', 5],
        ['9', 'Статус 9', now, 'fake-9', 5],
        ['8', 'Статус 8', now, 'fake-8', 5],
        ['4', 'Статус 1', now, 'fake-1', 5],
        ['1', 'Статус 1', now, 'fake-2', 5],
        ['3', 'Статус 3', now, 'fake-3', 7],
        ['1', 'Статус 4', now, 'fake-4', 7],
        ['8', 'Статус 5', now, 'fake-5', 7],
        ['7', 'Статус 7', now, 'fake-7', 8],
        ['5', 'Статус 8', now, 'fake-8', 7],
        ['9', 'Статус 9', now, 'fake-9', 1],
        ['8', 'Статус 8', now, 'fake-8', 7]
      ]
    }

    spinnerTestWrapper data
  end

  def advanced2(params)
    now = formatDate DateTime.now
    data = {
      title: 'advanced-2',
      headers: ['Тип', 'Описание', 'Дата', 'Фейк', 'Количество'],
      rows: [
        [nil, 'Статус 1', now, 1100, 1],
        [nil, 'Статус 41', now, 2380.0, 1],
        [nil, 'Статус 3', nil, 5500, 1],
        [nil, 'Статус 4', now, 1100, 1],
        [nil, 'Статус 5', now, 3770.0, 1],
        [123, 'Статус 7', now, 3370.0, 1],
        [nil, 'Статус 8', now, 1100, 1],
        [nil, 'Статус 9', now, 1100, 1],
        [nil, '{"utm_source":"instagram","utm_medium":"shapka","utm_campaign":"order","abs":{},"user_agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G36 Safari/601.1"}', now, 1100, 1],
        [nil, 'Статус 22', now, 1100, 1],
        [{}, 'Статус 21', now, 253220.0, 5],
        [[], 'Статус 20', now, 1100, 5],
        [nil, 'Статус 15', now, 1100, 5],
        [nil, 'Статус 5', now, 1100, 5],
        [nil, 'Статус 7', now, 1100, 5],
        ['nils', 'Статус 8', now, 1100, 5],
        [nil, 'Статус 9', now, 3770.0, 5],
        [nil, 'Статус 38', now, 1100, 5],
        ['', 'Статус 1', now, 1100, 5],
        [nil, 'Статус 16', now, 1100, 5],
        [nil, 'Статус 3', now,  266810.0, 7],
        [1.124, 'Статус 4', now, 1100, 7],
        [nil, 'Статус 5', now, 8250.0, 7],
        [:bla, 'Статус 7', now, 1100, 8],
        [nil, 'Статус 15', now, 990.0, 7],
        [nil, 'Статус 13', now, 1100, 1],
        [nil, 'Статус 12', now, 1100, 7]
      ]
    }
    spinnerTestWrapper data
  end

  def advanced3(params)
    {
      title: 'advanced-3',
      headers: ['Тип', 'Описание', 'boolean'],
      rows: [
        [1, 'Статус 1', false],
        [2, 'Статус 1', false],
        [3, 'Статус 3', true]
      ]
    }
  end

  def advanced4(params)
    {
      title: 'advanced-4',
      headers: ['Тип', 'Описание', 'boolean'],
      rows: [
        [18093430, 'Статус 3', true],
        ['', 'Статус 1', false],
        ['order', 'Статус 3', true],
        ['girl', 'Статус 1', false],
        ['', 'Статус 3', true],
        ['order', 'Статус 3', true],
        ['kinomania', 'Статус 3', true],
        ['kinomania', 'Статус 3', true],
        ['flatme', 'Статус 3', true],
        [18093430, 'Статус 3', true],
        ['psy.people', 'Статус 3', true],
        ['
              flatme', 'Статус 3', true],
        ['                man_x', 'Статус 3', true],
        ['girl', 'Статус 3', true],
        ['     boy', 'Статус 3', true],
        ['psy.people', 'Статус 3', true],
        ['    man_x', 'Статус 3', true],
        ['boy', 'Статус 3', true],
        [nil, 'Статус 3', true],
        ['          ', 'boy', true],
        [nil, 'Статус 3', true],
        [nil, 'Статус 3', true],
        ['boy2231', 'Статус 3', true],
        [false, 'Статус 7', true]
      ]
    }
  end

  def simplelivestream(params)
    now = formatDate DateTime.now

    {
      title: 'Статусы 22',
      titles: ['Тип', 'Описание', 'Дата', 'Фейк'],
      rows: [
        ['status1', "#{link_to 'Статус 1', '/meter'}", now, 'fake-1'],
        ['status1', 'Статус 1', now, 'fake-2'],
        ['status3', 'Статус 3', now, 'fake-3'],
        ['status4', 'Статус 4', now, 'fake-4'],
        ['status5', 'Статус 5', now, 'fake-5'],
        ['status7', 'Статус 7', now, 'fake-7'],
        ['status8', 'Статус 8', now, 'fake-8'],
        ['status9', 'Статус 9', now, 'fake-9'],
        ['status10', 'Статус 10', now, 'fake-10'],
        ['status8', 'Статус 8', now, 'fake-8'],
        ['status11', 'Статус 11', now, 'fake-11'],
        ['status12', 'Статус 12', now, 'fake-12']
      ]
    }
  end

  def binder(params)
    admin_id = params[:admin_id]

    if admin_id == '1'
      rows = [
        [4, 'Статус 4', false],
        [5, 'Статус 5', false],
        [6, 'Статус 6', true]
      ]
    else
      rows = [
        [1, 'Статус 1', false],
        [2, 'Статус 1', false],
        [3, 'Статус 3', true]
      ]
    end

    queries = [
      {
        name: :test_name,
        title: 'Деньги',
        options: [4500, 7500, 8000],
        value: 4500
      },
      {
        name: :test_name2,
        title: 'Сроки'
      },
      {
        name: :test_name3,
        title: 'Тип доставки'
      },
      {
        name: :test_name4,
        title: 'Просто длинная надпись'
      }
    ]

    {
      title: 'binder',
      headers: ['Тип', 'Описание', 'boolean'],
      rows: rows,
      queries: queries
    }
  end

  def error(params)
    now = formatDate DateTime.now

    {
      title: 'Статусы',
      headers: ['Тип', 'Описание', 'Дата'],
      rows: 0 + '' # error for testing
    }
  end

  def group_stacked(params)
    series = [{
      name: 'recent',
      data: [5, 3, 4, 7, 2]
    }, {
      name: 'approved',
      data: [3, 4, 4, 2, 5]
    }, {
      name: 'printed',
      data: [2, 5, 6, 2, 1]
    }, {
      name: 'ready',
      data: [3, 0, 4, 4, 3]
    }]

    {
      title: 'STACKED-COLUMN',
      formatter: '
        values = _.map(this.points, function(point) {
          href = "/admin/orders?utf8=✓&q%5Bstatus_eq%5D=" + point.series.name + "&q%5Bby_workday_in%5D=" + point.point.category + "&commit=Фильтровать&order=id_desc";
          return "<span style=\'color: " + point.series.color + "\'>\u25CF</span> <a class=\'stacked-chart__tooltip-link\' href=\'" + href + "\' style=\'color: " + point.series.color + "\' target=\'_blank\'>" + point.series.name + "</a> <b>" + point.y + "</b>";
        });
        values.join("<br />");
      ',
      bands: [{
        color: '#FCFFC5',
        points: [1, 3]
      }],
      categories: (1..5).to_a,
      series: series
    }
  end

  private

  def extract_dates_from_periods
    @periods.map(&:first)
  end

  def data_by_periods(data, period, type)
    from, to = period[0], period[1]
    data.select { |d| d[type] >= from and d[type] <= to }
  end

  def from_to_range
    from, to = if params.has_key?(:from)
      [Date.parse(params[:from]), Date.parse(params[:to])]
    else
      week = 6.day
      today = Time.current
      [today - week, today]
    end

    [
      Time.zone.local(from.year, from.month, from.day, 0, 0, 0),
      Time.zone.local(to.year,   to.month,   to.day, 23, 59, 59)
    ]
  end

  def formatted_periods
    day_format = '%d.%m.%y'
    @periods.map do |p|
      case @measure
        when 'days'
          p[0].strftime day_format
        when 'weeks'
          "#{p[0].strftime day_format} - #{p[1].strftime day_format}"
        when 'months'
          p[0].strftime '%b %y'
        else
          p[0].strftime day_format
      end
    end
  end

  def formatDate(date)
    day_format = '%d.%m.%y в %H:%M:%S'
    date.strftime day_format
  end

  def spinnerTestWrapper(data)
    sleep 1.5
    data
  end

  def get_columns(dates)
    if @measure == 'days'
      result = dates.each_index.select do |index|
        dates[index].saturday? || dates[index].sunday?
      end
    end
    result || []
  end

  def is_weekday(date)
    date.saturday? || date.sunday?
  end

end
