$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "meter/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
	s.name        = "meter"
	s.version     = Meter::VERSION
	s.authors     = ["lenarakhmadeev"]
	s.email       = ["lenarakhmadeev@gmail.com"]
	s.homepage    = "https://bitbucket.org/lenarakhmadeev/meter"
	s.summary     = "Summary of Meter."
	s.description = "Description of Meter."
	s.license     = "MIT"

	s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
	s.test_files = Dir["test/**/*"]

	s.add_dependency "rails", "> 3"
	s.add_dependency "sass-rails"
	s.add_dependency "coffee-rails"
	s.add_dependency "handlebars_assets", ">= 0.19.1"

	s.add_development_dependency "sqlite3"
end
